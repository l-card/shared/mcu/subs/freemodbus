#include <rtems.h>

#include "mb.h"
#include "mbport.h"


//==================================================================================================
static rtems_id qevent_id;


//==================================================================================================
//--------------------------------------------------------------------------------------------------
BOOL xMBPortEventGet(eMBEventType *event) {
    static const rtems_interval tout = 100;
    size_t msg_sz;

    return ((rtems_message_queue_receive(qevent_id, event, &msg_sz, RTEMS_WAIT, tout) ==
        RTEMS_SUCCESSFUL) && (msg_sz == sizeof(eMBEventType))) ? TRUE : FALSE;
}

//--------------------------------------------------------------------------------------------------
BOOL xMBPortEventInit(void) {
    static const uint32_t q_capacity = 1;

    return (rtems_message_queue_create(rtems_build_name('M', 'B', 'E', 'Q'), q_capacity,
        sizeof(eMBEventType), RTEMS_FIFO|RTEMS_LOCAL, &qevent_id) == RTEMS_SUCCESSFUL) ? TRUE :
        FALSE;
}

//--------------------------------------------------------------------------------------------------
BOOL xMBPortEventPost(eMBEventType event) {
    return (rtems_message_queue_send(qevent_id, &event, sizeof event) == RTEMS_SUCCESSFUL) ? TRUE :
        FALSE;
}
