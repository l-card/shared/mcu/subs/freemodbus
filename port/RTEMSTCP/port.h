/*
 * port.h
 *
 *  Created on: 06 нояб. 2013 г.
 *      Author: konstantin
 */

#ifndef PORT_H_
#define PORT_H_



#include <stdbool.h>


//==================================================================================================
#ifndef TRUE
#define TRUE (true)
#endif
#ifndef FALSE
#define FALSE (false)
#endif


#define assert(x)
#define ENTER_CRITICAL_SECTION()
#define EXIT_CRITICAL_SECTION()


//==================================================================================================
typedef bool BOOL;
typedef unsigned char UCHAR;
typedef char CHAR;
typedef unsigned short USHORT;
typedef short SHORT;
typedef unsigned long ULONG;
typedef long LONG;


#endif /* PORT_H_ */
