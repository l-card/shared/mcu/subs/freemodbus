#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include <rtems.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include "mbconfig.h"

#include "mb.h"
#include "mbport.h"


//==================================================================================================
#define MBAP_HEADER_LEN (7)
#define MBAP_MAX_LEN (MBAP_HEADER_LEN + 256)


//==================================================================================================
static void close_client_sock(void);
static inline bool invalid_sock(int h_sock);
static inline void invalidate_sock(int *h_sock);
static ssize_t readn(int h_sock, size_t n, void *vptr);
static rtems_task task_mbtcp(rtems_task_argument port);
static ssize_t writen(int h_sock, size_t n, const void *vptr);


//==================================================================================================
static rtems_id clientsock_semid;
static const unsigned short default_port = 502;
static int h_sock_client;
UCHAR mbap[MBAP_MAX_LEN];
static size_t mbap_len;
static const size_t mbapoffs_data = 7;
static const size_t mbapoffs_len = 4;
static const size_t mbapoffs_proto_id = 2;
static const size_t mbapoffs_transaction_id = 0;
static const size_t mbapoffs_unit_id = 6;
static rtems_id taskid_mbtcp;


//==================================================================================================
//--------------------------------------------------------------------------------------------------
static void close_client_sock(void) {
    rtems_semaphore_obtain(clientsock_semid, RTEMS_WAIT, RTEMS_NO_TIMEOUT);
    if (!invalid_sock(h_sock_client)) {
        close(h_sock_client);
        invalidate_sock(&h_sock_client);
    }
    rtems_semaphore_release(clientsock_semid);
}

//--------------------------------------------------------------------------------------------------
static inline bool invalid_sock(int h_sock) {
    return (h_sock < 0);
}

//--------------------------------------------------------------------------------------------------
static inline void invalidate_sock(int *h_sock) {
    *h_sock = -1;
}

//--------------------------------------------------------------------------------------------------
static ssize_t readn(int h_sock, size_t n, void *vptr) {
    size_t nleft = n;
    char *ptr = vptr;
    while (nleft > 0) {
        const ssize_t nread = read(h_sock, ptr, nleft);
        if (nread < 0)
            return -1;
        if (nread == 0)
            break;

        nleft -= nread;
        ptr += nread;
    }

    return n - nleft;             // return >= 0
}

//--------------------------------------------------------------------------------------------------
static rtems_task task_mbtcp(rtems_task_argument port) {
    // TCP/IP stack must be initialized!!!

    int h_sock;
    while ((h_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        rtems_task_wake_after(100);
    }

    struct sockaddr_in servaddr;
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    const unsigned short mbserver_port = (port == 0) ? default_port : port;
    servaddr.sin_port = htons(mbserver_port);
    memset(servaddr.sin_zero, '\0', sizeof servaddr.sin_zero);
    while (bind(h_sock, (struct sockaddr *)&servaddr, sizeof servaddr) < 0) {
        rtems_task_wake_after(10);
    }

    while (listen(h_sock, 3) < 0) {
        rtems_task_wake_after(10);
    }

    while (true) {
        struct sockaddr_in cliaddr;
        socklen_t addrlen = sizeof cliaddr;
        h_sock_client = accept(h_sock, (struct sockaddr *)&cliaddr, &addrlen);
        while (!invalid_sock(h_sock_client)) {
            if (readn(h_sock_client, MBAP_HEADER_LEN, mbap) != MBAP_HEADER_LEN)
                break;

            mbap_len = 0;

            static const size_t mbap_uid_len = 1;
            const size_t datalen = (((uint16_t)mbap[mbapoffs_len] << 8) |
                (uint16_t)mbap[mbapoffs_len+1]) - mbap_uid_len;
            if (datalen > MBAP_MAX_LEN - MBAP_HEADER_LEN)
                break;

            if (readn(h_sock_client, datalen, &mbap[mbapoffs_data]) != datalen)
                break;

            mbap_len = datalen + MBAP_HEADER_LEN;
            xMBPortEventPost(EV_FRAME_RECEIVED);
        }
        close_client_sock();
    }
}

//--------------------------------------------------------------------------------------------------
void vMBTCPPortDisable(void) {
    close_client_sock();
}

//--------------------------------------------------------------------------------------------------
BOOL xMBTCPPortGetRequest(UCHAR **frame, USHORT *len) {
    if (mbap_len <= 0)
        return FALSE;

    *frame = mbap;
    *len = mbap_len;

    mbap_len = 0;

    return TRUE;
}

//--------------------------------------------------------------------------------------------------
BOOL xMBTCPPortInit(USHORT port) {
    // TCP/IP stack must be initialized!!!

    rtems_semaphore_create(rtems_build_name('M', 'B', 'S', 'S'), 1,
        RTEMS_FIFO|RTEMS_SIMPLE_BINARY_SEMAPHORE|RTEMS_LOCAL, MBPORT_TCP_TASKPRI,
        &clientsock_semid);
    invalidate_sock(&h_sock_client);
    mbap_len = 0;

    return ((rtems_task_create(rtems_build_name('M', 'B', 'T', 'T'), MBPORT_TCP_TASKPRI,
        RTEMS_MINIMUM_STACK_SIZE,
        RTEMS_PREEMPT|RTEMS_NO_TIMESLICE|RTEMS_NO_ASR|RTEMS_INTERRUPT_LEVEL(0),
        RTEMS_DEFAULT_ATTRIBUTES, &taskid_mbtcp) == RTEMS_SUCCESSFUL) &&
        (rtems_task_start(taskid_mbtcp, task_mbtcp, port) == RTEMS_SUCCESSFUL));
}

//--------------------------------------------------------------------------------------------------
BOOL xMBTCPPortSendResponse(const UCHAR *frame, USHORT len) {
    if (writen(h_sock_client, len, frame) != len) {
        close_client_sock();
        return FALSE;
    }

    return TRUE;
}

//--------------------------------------------------------------------------------------------------
static ssize_t writen(int h_sock, size_t n, const void *vptr) {
    size_t nleft = n;
    const char *ptr = vptr;
    while (nleft > 0) {
        ssize_t nwritten = write(h_sock, ptr, nleft);
        if (nwritten < 0)
            return -1;

        nleft -= nwritten;
        ptr += nwritten;
    }
    return n;
}
