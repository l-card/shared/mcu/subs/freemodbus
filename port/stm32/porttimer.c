/*****************************************************************
 *
 *
 * 1.07.2011 - добавлена возможность выбора номера используемого таймера
 *             с помощью определения MB_PORT_TIMER_NUM (если не задано,
 *             то используется таймер 0 для совместимости с предыдущей
 *             версией файла
 *****************************************************************/

#include <stdbool.h>
#include "port.h"
#include "mb.h"
#include "mbport.h"
#include "mbconfig.h"
#include "chip.h"

#ifndef MB_PORT_TIMER_NUM
    #error modbus timer number must be specified
#endif

#define MB_TIMER_PREFIX                 TIM

#define MB_TIMER                        MB_TIMER_(MB_TIMER_PREFIX,MB_PORT_TIMER_NUM)
#define MB_TIMER_(pref, num)            MB_TIMER__(pref, num)
#define MB_TIMER__(pref, num)           CHIP_REGS_ ## pref ## num

#define MB_TIMER_PER_ID                 MB_TIMER_PER_ID_(MB_TIMER_PREFIX,MB_PORT_TIMER_NUM)
#define MB_TIMER_PER_ID_(pref, num)     MB_TIMER_PER_ID__(pref, num)
#define MB_TIMER_PER_ID__(pref, num)    CHIP_PER_ID_ ## pref ## num

#define MB_TIMER_CLK_FREQ               MB_TIMER_CLK_FREQ_(MB_TIMER_PREFIX,MB_PORT_TIMER_NUM)
#define MB_TIMER_CLK_FREQ_(pref, num)   MB_TIMER_CLK_FREQ__(pref, num)
#define MB_TIMER_CLK_FREQ__(pref, num)  CHIP_CLK_ ## pref ## num ## _KER_FREQ


#define MB_TIMER_ISR                    MB_TIMER_ISR_(MB_TIMER_PREFIX,MB_PORT_TIMER_NUM)
#define MB_TIMER_ISR_(pref, tnum)       MB_TIMER_ISR__(pref, tnum)
#define MB_TIMER_ISR__(pref, tnum)      pref##tnum##_IRQHandler

#define MB_TIMER_IRQ_NUM                MB_TIMER_IRQ_NUM_(MB_TIMER_PREFIX,MB_PORT_TIMER_NUM)
#define MB_TIMER_IRQ_NUM_(pref,tnum)    MB_TIMER_IRQ_NUM__(pref,tnum)
#define MB_TIMER_IRQ_NUM__(pref,tnum)   pref##tnum##_IRQn




/*================================================================================================*/
bool g_t0_inten = false;
/*================================================================================================*/


void mbPortTimerInterruptEnable(void) {
    NVIC_EnableIRQ( MB_TIMER_IRQ_NUM );
}

void mbPortTimerInterruptDisable(void) {
    NVIC_DisableIRQ( MB_TIMER_IRQ_NUM );
}

void mbTimerClose(void) {
    mbPortTimerInterruptDisable();

    /* возвращаем значения измененных регистров, сохраняя резервные биты */
    MB_TIMER->CR1 &= 0xF400;
    MB_TIMER->DIER = 0xA0A0;
    MB_TIMER->PSC = 0;
    MB_TIMER->CNT = 0;
    MB_TIMER->ARR = 0xFFFFFFFF;

    chip_per_clk_dis(MB_TIMER_PER_ID);
}


/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
 __attribute__((__interrupt__)) void MB_TIMER_ISR (void) {
    MB_TIMER->SR = 0;
    pxMBPortCBTimerExpired();
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
extern INLINE void vMBPortTimersDisable(void) {
    MB_TIMER->CR1 &= ~CHIP_REGFLD_TIM_CR1_CEN;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
extern INLINE void vMBPortTimersEnable(void) {
    MB_TIMER->CR1 &= ~CHIP_REGFLD_TIM_CR1_CEN;
    MB_TIMER->CNT = 0;
    MB_TIMER->SR = 0;
    MB_TIMER->CR1 |= CHIP_REGFLD_TIM_CR1_CEN;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL xMBPortTimersInit(USHORT usTimeOut50us) {
    chip_per_clk_en(MB_TIMER_PER_ID);
    chip_per_rst(MB_TIMER_PER_ID);

    MB_TIMER->CR1 = CHIP_REGFLD_TIM_CR1_URS | CHIP_REGFLD_TIM_CR1_OPM;

    MB_TIMER->PSC = (MB_TIMER_CLK_FREQ * 50 + 999999) / 1000000 - 1;
    MB_TIMER->CNT = 0;
    MB_TIMER->ARR = usTimeOut50us - 1;
    /* ручная генерация события для обновления настроек */
    MB_TIMER->EGR = CHIP_REGFLD_TIM_EGR_UG;

    MB_TIMER->DIER = CHIP_REGFLD_TIM_DIER_UIE;

    g_t0_inten = true;

    return TRUE;
}
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
