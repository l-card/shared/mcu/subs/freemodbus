#ifndef PORT_H_
#define PORT_H_


#include <assert.h>
#include <stdbool.h>
#include <stdint.h>



/*================================================================================================*/
#define INLINE
#define PR_BEGIN_EXTERN_C extern "C" {
#define PR_END_EXTERN_C   }

#define MB_PORT_HAS_CLOSE        (1)

#ifndef TRUE
#define TRUE true
#endif

#ifndef FALSE
#define FALSE false
#endif


#define ENTER_CRITICAL_SECTION() do {MBEnterCritical();} while (0);
#define EXIT_CRITICAL_SECTION()  do {MBExitCritical();} while (0);
/*================================================================================================*/

/*================================================================================================*/
typedef bool BOOL;

typedef unsigned char UCHAR;
typedef char CHAR;

typedef uint16_t USHORT;
typedef int16_t SHORT;

typedef uint32_t ULONG;
typedef int32_t LONG;

enum e_MBPortMode
    {
    e_MBPortMode_Serial,
    e_MBPortMode_TCP
    };
/*================================================================================================*/

/*================================================================================================*/
void MBEnterCritical(void);
void MBExitCritical(void);

/*================================================================================================*/

/*================================================================================================*/
extern enum e_MBPortMode g_mbport_mode;
/*================================================================================================*/

#endif //#ifndef PORT_H_
