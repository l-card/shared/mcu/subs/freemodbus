/**********************************************************************
 *
 * 1.07.2011 - Добавлены следующие возможности:
 *             Инверсия полярности сигнала управления направлением передачи
 *                 по RS-485 с помощью MB_PORT_RS485_DIR_INVERSE
 *                 (используется в E-124)
 *             Использование дробного делителя для задания частоты UART'а,
 *                 если определен MB_PORT_USE_FRACTIONAL_DIVIDER
 *             Явное запрещение приема данных, а не только прерываний
 *                 при вызове vMBPortSerialEnable с xRxEnable = FALSE
 **********************************************************************/


#include "port.h"
#include "chip.h"

#include "mb.h"
#include "mbport.h"
#include "mbconfig.h"

#ifndef MB_UART_LP
    #define MB_UART_LP 0
#endif

#ifndef MB_PORT_UART_NUM
    #error modbus uart port number must be specified
#endif


#ifdef CHIP_STM32G0XX
    #if MB_UART_LP
        #define MB_UART_PREFIX LPUART
    #else
        #define MB_UART_PREFIX USART
    #endif

    #define MB_UART_FEATURES               MB_UART_FEATURES_(MB_UART_PREFIX, MB_PORT_UART_NUM)
    #define MB_UART_FEATURES_(pref, num)   MB_UART_FEATURES__(pref, num)
    #define MB_UART_FEATURES__(pref, num)  CHIP_ ## pref ## num ## _FEATURES

    #if MB_UART_FEATURES == CHIP_UART_FEATURES_NOT_PRESENT
        #error specified UART is not present on selected device
    #elif MB_UART_FEATURES == CHIP_UART_FEATURES_BASIC
        #define MB_UART_SUPPORT_PRESCALER 0
        #define MB_UART_SUPPORT_FIFO      0
    #else
        #define MB_UART_SUPPORT_PRESCALER 1
        #define MB_UART_SUPPORT_FIFO      1
    #endif
#elif CHIP_STM32H7XX
    #if MB_UART_LP
        #if (MB_PORT_UART_NUM == 1)
            #define MB_UART_PREFIX LPUART
        #else
            #error Invalid LPUART number
        #endif
    #else
        #if (MB_PORT_UART_NUM == 1) || (MB_PORT_UART_NUM == 2) || (MB_PORT_UART_NUM == 3) || (MB_PORT_UART_NUM == 6)
            #define MB_UART_PREFIX  USART
        #elif (MB_PORT_UART_NUM == 4) || (MB_PORT_UART_NUM == 5) || (MB_PORT_UART_NUM == 7) || (MB_PORT_UART_NUM == 8)
            #define MB_UART_PREFIX  UART
        #else
            #error Invalid UART number
        #endif
    #endif
    #define MB_UART_SUPPORT_PRESCALER 1
    #define MB_UART_SUPPORT_FIFO      1
#endif


#if MB_UART_LP
    #define MB_UART_FREQ_DIV_MAX   0xFFFFF
    #define MB_UART_FREQ_DIV_MIN   0x300
    #define MB_UART_FREQ_MUL       256
#else
    #define MB_UART_FREQ_DIV_MAX   0xFFFF
    #define MB_UART_FREQ_DIV_MIN   16
    #define MB_UART_FREQ_MUL       1
#endif


#define MB_UART                        MB_UART_(MB_UART_PREFIX,MB_PORT_UART_NUM)
#define MB_UART_(pref, num)            MB_UART__(pref, num)
#define MB_UART__(pref, num)           CHIP_REGS_ ## pref ## num

#define MB_UART_PER_ID                 MB_UART_PER_ID_(MB_UART_PREFIX,MB_PORT_UART_NUM)
#define MB_UART_PER_ID_(pref, num)     MB_UART_PER_ID__(pref, num)
#define MB_UART_PER_ID__(pref, num)    CHIP_PER_ID_ ## pref ## num

#define MB_UART_CLK_FREQ               MB_UART_CLK_FREQ_(MB_UART_PREFIX,MB_PORT_UART_NUM)
#define MB_UART_CLK_FREQ_(pref, num)   MB_UART_CLK_FREQ__(pref, num)
#define MB_UART_CLK_FREQ__(pref, num)  CHIP_CLK_ ## pref ## num ## _KER_FREQ


#define MB_UART_ISR                    MB_UART_ISR_(MB_UART_PREFIX,MB_PORT_UART_NUM)
#define MB_UART_ISR_(pref, tnum)       MB_UART_ISR__(pref, tnum)
#define MB_UART_ISR__(pref, tnum)      pref##tnum##_IRQHandler

#define MB_UART_IRQ_NUM                MB_UART_IRQ_NUM_(MB_UART_PREFIX,MB_PORT_UART_NUM)
#define MB_UART_IRQ_NUM_(pref,tnum)    MB_UART_IRQ_NUM__(pref,tnum)
#define MB_UART_IRQ_NUM__(pref,tnum)   pref##tnum##_IRQn

#if MB_UART_SUPPORT_PRESCALER
static const uint16_t f_uart_presc_vals[] = {1, 2, 4, 6, 8, 10, 12, 16, 32, 64, 128, 256};
#endif



void mbTimerClose(void);

/*================================================================================================*/
static void prvvUARTRxISR(void);
static void prvvUARTTxReadyISR(void);
/*================================================================================================*/

/*================================================================================================*/
bool g_uart1_inten = false;
static volatile bool f_tx_en  =false;

static void prvvUARTRxISR(void) {
    pxMBFrameCBByteReceived();
}

static void prvvUARTTxReadyISR(void) {
    pxMBFrameCBTransmitterEmpty();
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
__attribute__((__interrupt__)) void MB_UART_ISR (void) {
    if (!f_tx_en && (MB_UART->ISR & CHIP_REGFLD_USART_ISR_RXFNE)) {
#ifdef MB_INDICATE_RX
        MB_INDICATE_RX();
#endif
        prvvUARTRxISR();
    } else {
        while (f_tx_en && (MB_UART->ISR & CHIP_REGFLD_USART_ISR_TXFNF)) {
            prvvUARTTxReadyISR();
        }
    }
}
/*------------------------------------------------------------------------------------------------*/


void mbPortSerialInterruptEnable(void) {
    NVIC_EnableIRQ( MB_UART_IRQ_NUM );
}

void mbPortSerialInterruptDisable(void) {
    NVIC_DisableIRQ( MB_UART_IRQ_NUM );
}


/*------------------------------------------------------------------------------------------------*/
void vMBPortClose(void) {
    vMBPortSerialEnable( FALSE, FALSE );
    mbPortSerialInterruptDisable();
    mbTimerClose();

    /* ожидание завершения передачи */
    while (!(MB_UART->ISR & CHIP_REGFLD_USART_ISR_TC))
        continue;

    /* запрет uart и сброс регистров c сохранением только резервных полей*/
    MB_UART->CR1 = 0;
    MB_UART->CR2 &= 0x00000086;
    MB_UART->CR3 &= 0x00010000;
    MB_UART->BRR &= 0xFFFF0000;
    MB_UART->PRESC &= 0xFFFFFFF0;

    chip_per_clk_dis(MB_UART_PER_ID);

    g_uart1_inten = false;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void vMBPortSerialEnable(BOOL xRxEnable, BOOL xTxEnable) {



    if (xRxEnable == TRUE) {
        MB_UART->CR1 |= CHIP_REGFLD_USART_CR1_RXFNEIE | CHIP_REGFLD_USART_CR1_RE;

    } else {
        MB_UART->CR1 &= ~CHIP_REGFLD_USART_CR1_RXFNEIE | CHIP_REGFLD_USART_CR1_RE;
    }

    f_tx_en = xTxEnable;
    if (xTxEnable == TRUE) {
        MB_UART->CR1 |= CHIP_REGFLD_USART_CR1_TXFNFIE;        
    } else {
        MB_UART->CR1 &= ~CHIP_REGFLD_USART_CR1_TXFNFIE;
    }
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL xMBPortSerialGetByte(CHAR *pucByte) {
    BOOL ok = (MB_UART->ISR & (CHIP_REGFLD_USART_ISR_PE | CHIP_REGFLD_USART_ISR_FE | CHIP_REGFLD_USART_ISR_NE)) == 0;
    uint8_t rd_byte = MB_UART->RDR & 0xFF;
    if (ok) {
        *pucByte = rd_byte;
    }
    return ok;
}

/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL xMBPortSerialInit(UCHAR
                       ucPort, ULONG ulBaudRate, UCHAR ucDataBits,  eMBParity eParity) {

    BOOL init_ok = TRUE;
    unsigned char m0 = 0, m1 = 0;
    unsigned char par_en = 0, par_sel = 0;

    chip_per_clk_en(MB_UART_PER_ID);
    chip_per_rst(MB_UART_PER_ID);

    /* настройка длины учитывает наличие бита четности */
    if (eParity != MB_PAR_NONE)
        ucDataBits++;

    if (ucDataBits == 8) {
        m0 = m1 = 0;
    } else if (ucDataBits == 9) {
        m1 = 0;
        m0 = 1;
    } else if (ucDataBits == 7) {
        m1 = 1;
        m0 = 0;
    } else {
        init_ok = FALSE;
    }

    if (init_ok) {
        if (eParity == MB_PAR_NONE) {
            par_en = par_sel = 0;
        } else if (eParity == MB_PAR_EVEN) {
            par_en = 1;
            par_sel = 0;
        } else if (eParity == MB_PAR_ODD) {
            par_en = 1;
            par_sel = 1;
        } else {
            init_ok = FALSE;
        }
    }

    if (init_ok) {
        unsigned uartdiv = (MB_UART_CLK_FREQ * MB_UART_FREQ_MUL  + ulBaudRate/2) / ulBaudRate;

        if (uartdiv < MB_UART_FREQ_DIV_MIN) {
            uartdiv = MB_UART_FREQ_DIV_MIN;
        }

#if MB_UART_SUPPORT_PRESCALER
        unsigned uart_presc_code = 0;


        for (unsigned char i = 0; (i < sizeof(f_uart_presc_vals)/sizeof(f_uart_presc_vals[0]))
             && (uartdiv > MB_UART_FREQ_DIV_MAX); ++i) {
            unsigned div = uartdiv / f_uart_presc_vals[i];
            if (div <= MB_UART_FREQ_DIV_MAX) {
                uart_presc_code = (char)i;
                uartdiv = div;
            }
        }
#endif

        if (uartdiv > MB_UART_FREQ_DIV_MAX) {
            init_ok = FALSE;
        } else {
#if MB_UART_SUPPORT_PRESCALER
            LBITFIELD_UPD(MB_UART->PRESC, CHIP_REGFLD_USART_PRESC_PRESCALER, uart_presc_code);
#endif
            MB_UART->BRR = uartdiv;
        }
    }

    if (init_ok) {
        CHIP_PIN_CONFIG(MB_PORT_UART_TX_PIN);
        CHIP_PIN_CONFIG(MB_PORT_UART_RX_PIN);


        /* сброс всех битов в неиспользуемых регистрах CR3 и CR2, кроме резервных */
        MB_UART->CR3 = MB_UART->CR3 & 0x00010000;
        MB_UART->CR2 = MB_UART->CR2 & 0x00000086;

        MB_UART->CR1 = 0
#if MB_UART_SUPPORT_FIFO
                | CHIP_REGFLD_USART_CR1_FIFOEN
#endif
                | CHIP_REGFLD_USART_CR1_TE
                | (m1 ? CHIP_REGFLD_USART_CR1_M1 : 0)
                | (m0 ? CHIP_REGFLD_USART_CR1_M0 : 0)
                | (par_en ? CHIP_REGFLD_USART_CR1_PCE : 0)
                | (par_sel ? CHIP_REGFLD_USART_CR1_PS : 0)
               ;
        MB_UART->CR1 |= CHIP_REGFLD_USART_CR1_UE;

        g_uart1_inten = true;
    }

    return init_ok;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL xMBPortSerialPutByte(CHAR ucByte) {
    MB_UART->TDR = LBITFIELD_SET(CHIP_REGFLD_USART_TDR_TDR, ucByte);
#ifdef MB_INDICATE_TX
    MB_INDICATE_TX();
#endif
    return TRUE;
}
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
