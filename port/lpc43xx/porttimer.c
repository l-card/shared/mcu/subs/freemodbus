/*****************************************************************
 *
 *
 * 1.07.2011 - добавлена возможность выбора номера используемого таймера
 *             с помощью определения MB_PORT_TIMER_NUM (если не задано,
 *             то используется таймер 0 для совместимости с предыдущей
 *             версией файла
 *****************************************************************/

#include <stdbool.h>
#include "port.h"
#include "mb.h"
#include "mbport.h"
#include "mbconfig.h"
#include "chip.h"



#define MB_TIMER_ISR            MB_TIMER_ISR_NUM(MB_PORT_TIMER_NUM)
#define MB_TIMER_ISR_NUM(tnum)  _MB_TIMER_ISR_NUM(tnum)
#define _MB_TIMER_ISR_NUM(tnum) TIMER##tnum##_IRQHandler

#define MB_TIMER               MB_TIMER_NUM(MB_PORT_TIMER_NUM)
#define MB_TIMER_NUM(tnum)     _MB_TIMER_NUM(tnum)
#define _MB_TIMER_NUM(tnum)    LPC_TIMER##tnum

#define MB_TIMER_IRQ              MB_TIMER_IRQ_NUM(MB_PORT_TIMER_NUM)
#define MB_TIMER_IRQ_NUM(tnum)   _MB_TIMER_IRQ_NUM(tnum)
#define _MB_TIMER_IRQ_NUM(tnum)   TIMER##tnum##_IRQn


#define MB_TIMER_CLK_MX              MB_TIMER_CLK_MX_NUM(MB_PORT_TIMER_NUM)
#define MB_TIMER_CLK_MX_NUM(tnum)    _MB_TIMER_CLK_MX_NUM(tnum)
#define _MB_TIMER_CLK_MX_NUM(tnum)   CLK_MX_TIMER##tnum





/*================================================================================================*/
bool g_t0_inten = false;
/*================================================================================================*/


void mbPortTimerInterruptEnable(void) {
    NVIC_EnableIRQ( MB_TIMER_IRQ );
}

void mbPortTimerInterruptDisable(void) {
    NVIC_DisableIRQ( MB_TIMER_IRQ );
}

void mbTimerClose(void) {
    mbPortTimerInterruptDisable();
    LPC_CCU_CFG(MB_TIMER_CLK_MX, 0);
}


/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
 __attribute__((__interrupt__)) void MB_TIMER_ISR (void) {
    MB_TIMER->IR = 0x3F;
    pxMBPortCBTimerExpired();
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
extern INLINE void vMBPortTimersDisable(void) {
    MB_TIMER->TCR = TIMER_RESET;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
extern INLINE void vMBPortTimersEnable(void) {
    MB_TIMER->TCR = TIMER_RESET;
    MB_TIMER->TCR = TIMER_ENABLE;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL xMBPortTimersInit(USHORT usTimeOut50us) {
    LPC_CCU_CFG(MB_TIMER_CLK_MX, 1);


    MB_TIMER->PR = 0;
    MB_TIMER->PC = 0;

    MB_TIMER->TC = 0;

    MB_TIMER->MR[0] = (LPC_SYSCLK / 20000) * usTimeOut50us;
    MB_TIMER->MCR = TIMER_INT_ON_MATCH(0) | TIMER_RESET_ON_MATCH(0);

    MB_TIMER->TCR = 0;
    MB_TIMER->CTCR = 0;

    mbPortTimerInterruptEnable();

    g_t0_inten = true;


    return TRUE;
}
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
