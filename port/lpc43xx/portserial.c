/**********************************************************************
 *
 * 1.07.2011 - Добавлены следующие возможности:
 *             Инверсия полярности сигнала управления направлением передачи
 *                 по RS-485 с помощью MB_PORT_RS485_DIR_INVERSE
 *                 (используется в E-124)
 *             Использование дробного делителя для задания частоты UART'а,
 *                 если определен MB_PORT_USE_FRACTIONAL_DIVIDER
 *             Явное запрещение приема данных, а не только прерываний
 *                 при вызове vMBPortSerialEnable с xRxEnable = FALSE
 **********************************************************************/


#include "port.h"
#include "chip.h"

#include "mb.h"
#include "mbport.h"
#include "lpc_uart_baudrate.inc"
#include "mbconfig.h"

#define MB_UART_ISR                 MB_UART_ISR_NUM(MB_PORT_UART_NUM)
#define MB_UART_ISR_NUM(tnum)       _MB_UART_ISR_NUM(tnum)
#define _MB_UART_ISR_NUM(tnum)      UART##tnum##_IRQHandler

#define MB_UART                     MB_UART_NUM(MB_PORT_UART_NUM)
#define MB_UART_NUM(tnum)           _MB_UART_NUM(tnum)
#if MB_PORT_UART_NUM == 1
    #define _MB_UART_NUM(tnum)          LPC_UART##tnum
#else
    #define _MB_UART_NUM(tnum)          LPC_USART##tnum
#endif

#define MB_UART_CLK_MX              MB_UART_CLK_MX_NUM(MB_PORT_UART_NUM)
#define MB_UART_CLK_MX_NUM(tnum)    _MB_UART_CLK_MX_NUM(tnum)
#define _MB_UART_CLK_MX_NUM(tnum)   CLK_MX_UART##tnum

#define MB_UART_CLK_BASE            MB_UART_CLK_BASE_NUM(MB_PORT_UART_NUM)
#define MB_UART_CLK_BASE_NUM(tnum)  _MB_UART_CLK_BASE_NUM(tnum)
#define _MB_UART_CLK_BASE_NUM(tnum) CLK_BASE_UART##tnum

#define MB_UART_CLK_CFG             MB_UART_CLK_CFG_NUM(MB_PORT_UART_NUM)
#define MB_UART_CLK_CFG_NUM(tnum)   _MB_UART_CLK_CFG_NUM(tnum)
#define _MB_UART_CLK_CFG_NUM(tnum)  LPC_BASE_UART##tnum##_CFG


#define MB_UART_IRQ              MB_UART_IRQ_NUM(MB_PORT_UART_NUM)
#define MB_UART_IRQ_NUM(tnum)   _MB_UART_IRQ_NUM(tnum)
#if MB_PORT_UART_NUM == 1
    #define _MB_UART_IRQ_NUM(tnum)   UART##tnum##_IRQn
#else
    #define _MB_UART_IRQ_NUM(tnum)   USART##tnum##_IRQn
#endif



void mbTimerClose(void);

/*================================================================================================*/
static void prvvUARTRxISR(void);
static void prvvUARTTxReadyISR(void);
/*================================================================================================*/

/*================================================================================================*/
bool g_uart1_inten = false;

static void prvvUARTRxISR(void) {
    pxMBFrameCBByteReceived();
}

static void prvvUARTTxReadyISR(void) {
    pxMBFrameCBTransmitterEmpty();
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
__attribute__((__interrupt__)) void MB_UART_ISR (void) {
    unsigned long iir;
    while (!((iir = MB_UART->IIR) & UART_IIR_INTSTAT_PEND)) {
        switch (iir & UART_IIR_INTID_MASK) {
            case UART_IIR_INTID_RLS:
                (volatile void)MB_UART->LSR;
                break;
            case UART_IIR_INTID_RDA:
#ifdef MB_INDICATE_RX
                MB_INDICATE_RX();
#endif
                /* нет break! */
            case UART_IIR_INTID_CTI:
                prvvUARTRxISR();
                break;
            case UART_IIR_INTID_THRE:
                prvvUARTTxReadyISR();
                break;
            default:
                break;
        }
    }
}
/*------------------------------------------------------------------------------------------------*/


void mbPortSerialInterruptEnable(void) {
    NVIC_EnableIRQ( MB_UART_IRQ );
}

void mbPortSerialInterruptDisable(void) {
    NVIC_DisableIRQ( MB_UART_IRQ );
}


/*------------------------------------------------------------------------------------------------*/
void vMBPortClose(void) {
    vMBPortSerialEnable( FALSE, FALSE );
    mbPortSerialInterruptDisable();
    mbTimerClose();
    g_uart1_inten = false;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void vMBPortSerialEnable(BOOL xRxEnable, BOOL xTxEnable) {
    if (xRxEnable == TRUE) {
        MB_UART->IER |= UART_IER_RBRINT;
        MB_UART->RS485CTRL &= ~UART_RS485CTRL_RX_DIS;
    } else {
        MB_UART->IER &= ~UART_IER_RBRINT;
        MB_UART->RS485CTRL |= UART_RS485CTRL_RX_DIS;
    }

    if (xTxEnable == TRUE) {
        MB_UART->IER |= UART_IER_THREINT;
        while (!(MB_UART->LSR & UART_LSR_THRE)) {
            continue;
        }
        prvvUARTTxReadyISR();
    } else {
        MB_UART->IER &= ~UART_IER_THREINT;
    }
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL xMBPortSerialGetByte(CHAR *pucByte) {
    *pucByte = MB_UART->RBR;
    return TRUE;
}

/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL xMBPortSerialInit(UCHAR ucPort, ULONG ulBaudRate, UCHAR ucDataBits,  eMBParity eParity) {
    BOOL init_ok = TRUE;
    unsigned cfg = 0;

    switch (ucDataBits) {
        case 5:
            cfg |= UART_LCR_WLEN5;
            break;
        case 6:
            cfg |= UART_LCR_WLEN6;
            break;
        case 7:
            cfg |= UART_LCR_WLEN7;
            break;
        case 8:
            cfg |= UART_LCR_WLEN8;
            break;
        default:
            init_ok = FALSE;
            break;
    }

    if (init_ok) {
        switch(eParity) {
            case MB_PAR_NONE:
                cfg |= UART_LCR_PARITY_DIS;
                break;
            case MB_PAR_ODD:
                cfg |= UART_LCR_PARITY_EN | UART_LCR_PARITY_ODD;
                break;
            case MB_PAR_EVEN:
                cfg |= UART_LCR_PARITY_EN | UART_LCR_PARITY_EVEN;
                break;
            default:
                init_ok = FALSE;
                break;
        }
    }

    if (init_ok)  {
        LPC_CGU->BASE_CLK[MB_UART_CLK_BASE] = MB_UART_CLK_CFG;
        lpc_delay_clk(20);
        LPC_CCU_CFG(MB_UART_CLK_MX, 1);
        lpc_delay_clk(20);

        MB_UART->LCR = cfg;
        MB_UART->IER = 0;


        unsigned dl;
        unsigned fract_div;
        unsigned fract_mul;
        f_get_baudrate_settings(LPC_CLKF_BASE(MB_UART_CLK_CFG), ulBaudRate, &dl, &fract_div, &fract_mul );

        if (fract_mul != 0) {
            MB_UART->LCR |= UART_LCR_DLAB_EN;
            MB_UART->DLL = dl & 0xFF;
            MB_UART->DLM = (dl >> 8) & 0xFF;
            MB_UART->FDR =UART_FDR_DIVADDVAL(fract_div) |
                         UART_FDR_MULVAL(fract_mul);
            MB_UART->LCR &= ~UART_LCR_DLAB_EN;
            MB_UART->FCR = UART_FCR_FIFO_EN | UART_FCR_RX_RS | UART_FCR_TX_RS;;
            MB_UART->FCR = UART_FCR_FIFO_EN;

            (volatile void)MB_UART->IIR;



            MB_UART->RS485CTRL = UART_RS485CTRL_DCTRL_EN;
#ifdef MB_PORT_RS485_DIR_DTR
            MB_UART->RS485CTRL |= UART_RS485CTRL_SEL_DTR;
#endif
#ifdef MB_PORT_RS485_DIR_INVERSE
            MB_UART->RS485CTRL |= UART_RS485CTRL_OINV_1;
#endif

#if defined MB_UART_PIN_DIR
            CHIP_PIN_CONFIG(MB_UART_PIN_DIR, 0, CHIP_PIN_MODE_FAST | CHIP_PIN_MODE_NOPULL);
#endif
            CHIP_PIN_CONFIG(MB_UART_PIN_RX, 1, CHIP_PIN_MODE_FAST | CHIP_PIN_MODE_NOPULL);
            CHIP_PIN_CONFIG(MB_UART_PIN_TX, 0, CHIP_PIN_MODE_FAST | CHIP_PIN_MODE_NOPULL);

            mbPortSerialInterruptEnable();
            g_uart1_inten = true;
        } else {
            init_ok = FALSE;
        }
    }

    return init_ok;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL xMBPortSerialPutByte(CHAR ucByte) {
    MB_UART->THR = ucByte;
#ifdef MB_INDICATE_TX
    MB_INDICATE_TX();
#endif
    return TRUE;
}
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
