/*
 * ���������� MODBUS-TCP ����� ��� ����� uIP ���������������� LPC1766.
 */
#include "port.h"
#include "mb.h"
#include "mbport.h"
#include "porttcp.h"

#if MB_TCP_ENABLED
#include "uip.h"


/*================================================================================================*/
#define MB_TCP_BUF_SIZE (256 + 7)           /* Must hold a complete Modbus TCP frame. */
#define MB_TCP_DEFAULT_PORT (502)           /* TCP listening port. */

/* MBAP Header */
#define MB_TCP_UID (6)
#define MB_TCP_LEN (4)
#define MB_TCP_FUNC (7)
/*================================================================================================*/

/*================================================================================================*/
static void f_handle_input(void);
static void f_handle_output(void);
static void f_init_input(void);
static void f_init_output(void);
/*================================================================================================*/

/*================================================================================================*/
static int f_cnt_close = 0;
static const int f_ct_poll_tout = 60;
static bool f_mbtcp_enable;
static uint16_t f_MBTCP_port;
static int f_poll_counter;
static bool f_send_inprogress;
static bool f_tcpclose_requested = false;
static UCHAR f_TCP_rxbuf[ MB_TCP_BUF_SIZE ];
static int f_TCP_rxbufpos;
static UCHAR f_TCP_txbuf[ MB_TCP_BUF_SIZE ];
static int f_TCP_txbuflen;
static int f_TCP_txbufpos;
uip_ipaddr_t g_mbtcp_ripaddr;
bool g_tcp_poll_expired;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
static void f_handle_input
    (
    void
    )
    {
    /*
     * ��������� ������ �� TCP.
     */
    if (!uip_newdata())
        return;

    f_poll_counter = 0;

    if (uip_datalen() > MB_TCP_BUF_SIZE - f_TCP_rxbufpos)
        {
        // ������� ������� ����� ������. �������� ����� ������.
        f_init_input();
        return;
        }

    for (int i = 0; (i < uip_datalen()); i++)
        {
        f_TCP_rxbuf[ f_TCP_rxbufpos ] = ((uint8_t *)uip_appdata)[ i ];
        f_TCP_rxbufpos++;
        }
    if (f_TCP_rxbufpos >= MB_TCP_FUNC)
        {
        // ������ ��� �������. ����������� ����� ����������� ������.
        uint16_t len = ((uint16_t)f_TCP_rxbuf[ MB_TCP_LEN ] << 8) |
            (uint16_t)f_TCP_rxbuf[ MB_TCP_LEN+1 ];
        if (len > MB_TCP_BUF_SIZE - 6)
            {
            // ����� ����������� ������ ������� �������. �������� ����� ������ ������.
            f_init_input();
            }
        else if (f_TCP_rxbufpos >= MB_TCP_UID + len)
            {
            // ������� ��� ������. ������������� �� ����.
            xMBPortEventPost( EV_FRAME_RECEIVED );
            }
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_handle_output
    (
    void
    )
    {
    /*
     * ���������� �������� ������ �� TCP.
     * ��������
     *   ������������ ����� ��������, ����������� � �������� ������������ �� ����� ������.
     */
    if (uip_rexmit())
        {
        int len = (f_TCP_txbuflen <= uip_mss()) ? f_TCP_txbuflen : uip_mss();
        uip_send( &f_TCP_txbuf[ f_TCP_txbufpos ], len );

        f_poll_counter = 0;
        }
    else if (uip_acked())
        {
        int len = (f_TCP_txbuflen <= uip_mss()) ? f_TCP_txbuflen : uip_mss();
        f_TCP_txbufpos += len;
        f_TCP_txbuflen -= len;
        if (f_TCP_txbuflen > 0)
            {
            len = (f_TCP_txbuflen <= uip_mss()) ? f_TCP_txbuflen : uip_mss();
            uip_send( &f_TCP_txbuf[ f_TCP_txbufpos ], len );
            }
        else
            {
            f_send_inprogress = false;
            }

        f_poll_counter = 0;
        }
    else if (!f_send_inprogress && (f_TCP_txbuflen > 0))
        {
        int len = (f_TCP_txbuflen <= uip_mss()) ? f_TCP_txbuflen : uip_mss();
        f_TCP_txbufpos = 0;
        uip_send( &f_TCP_txbuf[ f_TCP_txbufpos ], len );
        f_send_inprogress = true;

        f_poll_counter = 0;
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_init_input
    (
    void
    )
    {
    /*
     * ������������� ������ ������ �� TCP.
     */
    f_send_inprogress = false;
    f_TCP_rxbufpos = 0;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
static void f_init_output
    (
    void
    )
    {
    f_TCP_txbuflen = 0;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void mbtcp_poll
    (
    void
    )
    {
    f_mbtcp_enable = true;
    f_tcpclose_requested = false;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void portmbtcp_appcall
    (
    void
    )
    {
    /*
     * ��������� ������� �� uIP.
     * ��������
     *   ������������ ���� ��������� �� TCP ������ � ������������ � FreeMODBUS �� �������� ������.
     *   ��� ������������� ������������ ��������.
     */
    if (!f_mbtcp_enable)
        return;

    if (uip_aborted() || uip_timedout())
        {
        // ���������� ��������� - ��������� ������������� ����� ����������
        g_tcp_poll_expired = false;
        if (!f_tcpclose_requested)
            {
            uip_listen( htons( f_MBTCP_port ) );
            }
        else
            {
            f_tcpclose_requested = false;
            f_mbtcp_enable = false;
            }
        f_cnt_close = 0;
        }
    else if (uip_closed())
        {
        if (++f_cnt_close >= 2)
            {
            g_tcp_poll_expired = false;
            if (!f_tcpclose_requested)
                {
                uip_listen( htons( f_MBTCP_port ) );
                }
            else
                {
                f_tcpclose_requested = false;
                f_mbtcp_enable = false;
                }
            f_cnt_close = 0;
            }
        }
    else if (f_tcpclose_requested)
        {
        uip_abort();
        f_tcpclose_requested = false;
        f_mbtcp_enable = false;
        }
    else if (uip_poll() && (++f_poll_counter >= f_ct_poll_tout))
        {
        // � ���������� ������� ����� ������ �� ��������. ��������� ���������� � ���� ����������.
        g_tcp_poll_expired = false;
        uip_abort();
        uip_listen( htons( f_MBTCP_port ));
        }
    else
        {
        if (uip_connected())
            {
            // ���������� ����������� - ��������� ��������� ����� ������� �� ����������
            g_tcp_poll_expired = false;
            uip_unlisten( htons( f_MBTCP_port ) );
            f_init_input();
            f_init_output();
            f_poll_counter = 0;
            uip_ipaddr_copy( g_mbtcp_ripaddr, uip_conn->ripaddr );
            }
        f_handle_input();
        f_handle_output();
        }
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void vMBTCPPortClose
    (
    void
    )
    {
    g_tcp_poll_expired = true;
    f_tcpclose_requested = true;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void vMBTCPPortDisable
    (
    void
    )
    {
    g_tcp_poll_expired = true;
    f_tcpclose_requested = true;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL xMBTCPPortGetRequest
    (
    UCHAR **ppucMBTCPFrame,
    USHORT *usTCPLength
    )
    {
    *ppucMBTCPFrame = &f_TCP_rxbuf[ 0 ];
    *usTCPLength = (((USHORT)f_TCP_rxbuf[ MB_TCP_LEN ] << 8) |
        (USHORT)f_TCP_rxbuf[ MB_TCP_LEN+1 ]) + 6;

    /* Reset the buffer. */
    f_init_input();
    return TRUE;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL xMBTCPPortInit
    (
    USHORT usTCPPort
    )
    {
    /*
     * ������������� ����������� TCP/IP.
     * ��������
     *   ���������� ������������� ����� MODBUS. ������������� ����� uIP ������ ���� ��� ��������� �
     *   ���������� �������.
     */
    if(usTCPPort == 0)
        {
        f_MBTCP_port = MB_TCP_DEFAULT_PORT;
        }
        else
        {
        f_MBTCP_port = usTCPPort;
        }
    uip_listen( htons( f_MBTCP_port ) );
    f_init_output();
    g_mbport_mode = e_MBPortMode_TCP;
    f_tcpclose_requested = false;
    f_mbtcp_enable = true;
    f_cnt_close = 0;
    uip_ipaddr( g_mbtcp_ripaddr, 0, 0, 0, 0 );

    return TRUE;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL xMBTCPPortSendResponse
    (
    const UCHAR *pucMBTCPFrame,
    USHORT usTCPLength
    )
    {
    if (usTCPLength > MB_TCP_BUF_SIZE)
        return FALSE;

    for (int i = 0; (i < usTCPLength); i++)
        {
        f_TCP_txbuf[ i ] = pucMBTCPFrame[ i ];
        }
    f_TCP_txbufpos = 0;
    f_TCP_txbuflen = usTCPLength;
    g_tcp_poll_expired = true;

    return TRUE;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/

#endif
