/*****************************************************************
 *
 *
 * 1.07.2011 - добавлена возможность выбора номера используемого таймера
 *             с помощью определения MB_PORT_TIMER_NUM (если не задано,
 *             то используется таймер 0 для совместимости с предыдущей
 *             версией файла
 *****************************************************************/

#include <stdbool.h>

#include "LPC17xx.h"
#include "iolpc17XX.h"

#include "port.h"

#include "mb.h"
#include "mbport.h"



#ifndef MB_PORT_TIMER_NUM
    #define MB_PORT_TIMER_NUM 0
#endif

#define MB_TIMER_ISR            MB_TIMER_ISR_NUM(MB_PORT_TIMER_NUM)
#define MB_TIMER_ISR_NUM(tnum)  _MB_TIMER_ISR_NUM(tnum)
#define _MB_TIMER_ISR_NUM(tnum) TIMER##tnum##_IRQHandler

#define MB_TIMER               MB_TIMER_NUM(MB_PORT_TIMER_NUM)
#define MB_TIMER_NUM(tnum)     _MB_TIMER_NUM(tnum)
#define _MB_TIMER_NUM(tnum)    LPC_TIM##tnum

#define MB_TIMER_IRQ              MB_TIMER_IRQ_NUM(MB_PORT_TIMER_NUM)
#define MB_TIMER_IRQ_NUM(tnum)   _MB_TIMER_IRQ_NUM(tnum)
#define _MB_TIMER_IRQ_NUM(tnum)   TIMER##tnum##_IRQn



/*================================================================================================*/
bool g_t0_inten = false;
/*================================================================================================*/



/*================================================================================================*/

void mbPortTimerInterruptEnable(void) {
    NVIC_EnableIRQ( MB_TIMER_IRQ );
}

void mbPortTimerInterruptDisable(void) {
    NVIC_DisableIRQ( MB_TIMER_IRQ );
}

/*------------------------------------------------------------------------------------------------*/
 __attribute__((__interrupt__)) void MB_TIMER_ISR(void) {
     MB_TIMER->IR = 0x3F;
    (void)pxMBPortCBTimerExpired();
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
extern INLINE void vMBPortTimersDisable(void ) {
    MB_TIMER->TCR = LPC_TIM_TCR_CounterReset_Msk;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
extern INLINE void vMBPortTimersEnable(void) {
    MB_TIMER->TCR = LPC_TIM_TCR_CounterReset_Msk;
    MB_TIMER->TCR = LPC_TIM_TCR_CounterEnable_Msk;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL xMBPortTimersInit(USHORT usTimeOut50us) {
    MB_TIMER->PR = 0;
    MB_TIMER->PC = 0;

    MB_TIMER->TC = 0;

    MB_TIMER->MR0 = (TIMER_CLK / 20000) * usTimeOut50us;
    MB_TIMER->MCR = LPC_TIM_MCR_MR0I_Msk | LPC_TIM_MCR_MR0R_Msk;

    MB_TIMER->TCR = 0;
    MB_TIMER->CTCR = 0;

    mbPortTimerInterruptEnable();

    g_t0_inten = true;


    return TRUE;
}

void xMBPortTimersClose(void) {
    g_t0_inten = false;
    mbPortTimerInterruptDisable();

    MB_TIMER->TCR = 0;
    MB_TIMER->TC = 0;
    MB_TIMER->PR = 0;
    MB_TIMER->PC = 0;

    MB_TIMER->MR0 = 0;
    MB_TIMER->MCR = 0;

    MB_TIMER->IR = 0x3F;

}
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
