#include "port.h"
#include "mb.h"

void mbPortSerialInterruptEnable(void);
void mbPortSerialInterruptDisable(void);
void mbPortTimerInterruptEnable(void);
void mbPortTimerInterruptDisable(void);

extern bool g_uart1_inten;
extern bool g_t0_inten;

/*================================================================================================*/
enum e_MBPortMode g_mbport_mode;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
void MBEnterCritical(void) {
    mbPortSerialInterruptDisable();
    mbPortTimerInterruptDisable();
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void MBExitCritical(void) {
    if (g_uart1_inten)
        mbPortSerialInterruptEnable();

    if (g_t0_inten)
        mbPortTimerInterruptEnable();
}
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
