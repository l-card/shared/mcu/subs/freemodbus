/**********************************************************************
 *
 * 1.07.2011 - Добавлены следующие возможности:
 *             Инверсия полярности сигнала управления направлением передачи
 *                 по RS-485 с помощью MB_PORT_RS485_DIR_INVERSE
 *                 (используется в E-124)
 *             Использование дробного делителя для задания частоты UART'а,
 *                 если определен MB_PORT_USE_FRACTIONAL_DIVIDER
 *             Явное запрещение приема данных, а не только прерываний
 *                 при вызове vMBPortSerialEnable с xRxEnable = FALSE
 **********************************************************************/

#include "LPC17xx.h"
#include "core_cm3.h"
#include "iolpc17XX.h"

#include "port.h"

#include "mb.h"
#include "mbport.h"
#include "lpc_uart_baudrate.inc"

#ifndef MB_PORT_UART_NUM
    #define MB_PORT_UART_NUM 1
#endif

#define MB_UART_ISR            MB_UART_ISR_NUM(MB_PORT_UART_NUM)
#define MB_UART_ISR_NUM(tnum)  _MB_UART_ISR_NUM(tnum)
#define _MB_UART_ISR_NUM(tnum) UART##tnum##_IRQHandler

#define MB_UART               MB_UART_NUM(MB_PORT_UART_NUM)
#define MB_UART_NUM(tnum)     _MB_UART_NUM(tnum)
#define _MB_UART_NUM(tnum)    LPC_UART##tnum

#define MB_UART_IRQ              MB_UART_IRQ_NUM(MB_PORT_UART_NUM)
#define MB_UART_IRQ_NUM(tnum)   _MB_UART_IRQ_NUM(tnum)
#define _MB_UART_IRQ_NUM(tnum)   UART##tnum##_IRQn


/*================================================================================================*/
static void prvvUARTRxISR(void);
static void prvvUARTTxReadyISR(void);
/*================================================================================================*/

/*================================================================================================*/
bool g_uart1_inten = false;
#if defined MB_PORT_RS485_MANUAL_DIRCTL && defined MB_PORT_RS485_MANUAL_DIR_CHECK
static bool g_uart_wt_tx = false;
#endif

/*================================================================================================*/

/*================================================================================================*/


void mbPortSerialInterruptEnable(void) {
    NVIC_EnableIRQ( MB_UART_IRQ );
}

void mbPortSerialInterruptDisable(void) {
    NVIC_DisableIRQ( MB_UART_IRQ );
}


static void prvvUARTRxISR(void) {
    pxMBFrameCBByteReceived();
}

static void prvvUARTTxReadyISR(void) {
    pxMBFrameCBTransmitterEmpty();
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
__attribute__((__interrupt__)) void MB_UART_ISR(void) {
    unsigned long iir;

    while (!((iir = MB_UART->IIR) & LPC_UART_IIR_IntStatus_Msk)) {
        switch ((iir & LPC_UART_IIR_IntId_Msk) >> LPC_UART_IIR_IntId_Pos) {
            case 0x03:
                (volatile void)MB_UART->LSR;
                break;
            case 0x02:
            case 0x06:
#ifdef MB_INDICATE_RX
                MB_INDICATE_RX();
#endif
                prvvUARTRxISR();
                break;
            case 0x01:
                prvvUARTTxReadyISR();
                break;
            default:
                break;
        }
    }
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void vMBPortClose(void) {
    vMBPortSerialEnable( FALSE, FALSE );
    mbPortSerialInterruptDisable();

    xMBPortTimersClose();

    g_uart1_inten = false;
    MB_UART->LCR |= LPC_UART_LCR_DLAB_Msk;
    MB_UART->DLL = 1;
    MB_UART->DLM = 0;
    MB_UART->FDR = 1 << 4;
    MB_UART->LCR = 0;

    MB_UART->IER = 0;
    (volatile void)MB_UART->IIR;
    (volatile void)MB_UART->LSR;
    MB_UART->FCR = LPC_UART_FCR_FIFOEnable_Msk | LPC_UART_FCR_RXFIFOReset_Msk |
        LPC_UART_FCR_TXFIFOReset_Msk;
    MB_UART->FCR = 0;
#if MB_PORT_UART_NUM == 1
    MB_UART->RS485CTRL = 0;
#endif
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
void vMBPortSerialEnable(BOOL xRxEnable, BOOL xTxEnable) {
    if (xRxEnable == TRUE) {
        MB_UART->IER |= LPC_UART_IER_RBRInterruptEnable_Msk;
#if MB_PORT_UART_NUM == 1
        MB_UART->RS485CTRL &= ~LPC_UART1_RS485CTRL_RXDIS_Msk;
#endif
    } else {
        MB_UART->IER &= ~LPC_UART_IER_RBRInterruptEnable_Msk;
#if MB_PORT_UART_NUM == 1
        MB_UART->RS485CTRL |= LPC_UART1_RS485CTRL_RXDIS_Msk;
#endif
    }

    if (xTxEnable == TRUE) {
        MB_UART->IER |= LPC_UART_IER_THREInterruptEnable_Msk;
        while (!(MB_UART->LSR & LPC_UART_LSR_THRE_Msk)) {
            continue;
        }
#ifdef MB_PORT_RS485_MANUAL_DIRCTL
        MB_PORT_RS485_MANUAL_DIRSET_TX();
#endif
        prvvUARTTxReadyISR();
    } else {
        MB_UART->IER &= ~LPC_UART_IER_THREInterruptEnable_Msk;
#ifdef MB_PORT_RS485_MANUAL_DIRCTL
#ifdef MB_PORT_RS485_MANUAL_DIR_CHECK
        g_uart_wt_tx = true;
#else
        while (!(MB_UART->LSR & LPC_UART_LSR_TEMT_Msk)) {
            continue;
        }
        MB_PORT_RS485_MANUAL_DIRSET_RX();
#endif
#endif
    }
}
/*------------------------------------------------------------------------------------------------*/

#if defined MB_PORT_RS485_MANUAL_DIRCTL  && defined MB_PORT_RS485_MANUAL_DIR_CHECK
void xMBPortSerialDirCheck(void) {
    if (g_uart_wt_tx && (MB_UART->LSR & LPC_UART_LSR_TEMT_Msk)) {
       g_uart_wt_tx = FALSE;
       MB_PORT_RS485_MANUAL_DIRSET_RX();
    }
}

#endif


/*------------------------------------------------------------------------------------------------*/
BOOL xMBPortSerialGetByte (CHAR *pucByte) {
    *pucByte = MB_UART->RBR;
    return TRUE;
}
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL xMBPortSerialInit(UCHAR ucPort, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity) {
    BOOL init_ok = TRUE;

    unsigned long cfg = 0;

    switch (ucDataBits) {
        case 5:
            break;
        case 6:
            cfg |= 0x01 << LPC_UART_LCR_WordLengthSelect_Pos;
            break;
        case 7:
            cfg |= 0x02 << LPC_UART_LCR_WordLengthSelect_Pos;
            break;
        case 8:
            cfg |= 0x03 << LPC_UART_LCR_WordLengthSelect_Pos;
            break;
        default:
            init_ok = FALSE;
    }

    switch(eParity) {
        case MB_PAR_NONE:
            break;
        case MB_PAR_ODD:
            cfg |= LPC_UART_LCR_ParityEnable_Msk;
            break;
        case MB_PAR_EVEN:
            cfg |= LPC_UART_LCR_ParityEnable_Msk | (0x01 << LPC_UART_LCR_ParitySelect_Pos);
            break;
        default:
            init_ok = FALSE;
    }

    if (init_ok) {
        MB_UART->LCR = cfg;
        MB_UART->IER = 0;
#ifndef MB_PORT_RS485_MANUAL_DIRCTL
#if MB_PORT_UART_NUM == 1
        MB_UART->RS485CTRL = LPC_UART1_RS485CTRL_DCTRL_Msk;
#ifndef MB_PORT_RS485_DIR_RTS
        MB_UART->RS485CTRL |= LPC_UART1_RS485CTRL_SEL_Msk;
#endif
#ifdef MB_PORT_RS485_DIR_INVERSE
        MB_UART->RS485CTRL |= LPC_UART1_RS485CTRL_OINV_Msk;
#endif
#endif
#endif

        unsigned dl;
        unsigned fract_div;
        unsigned fract_mul;
        f_get_baudrate_settings(UART_CLK, ulBaudRate, &dl, &fract_div, &fract_mul );
        if (fract_mul != 0) {
            MB_UART->LCR |= LPC_UART_LCR_DLAB_Msk;
            MB_UART->DLL = dl & 0xFF;
            MB_UART->DLM = (dl >> 8) & 0xFF;
            MB_UART->FDR =((fract_div << LPC_UART_FDR_DIVADDVAL_Pos)
                    & LPC_UART_FDR_DIVADDVAL_Msk) |
                    ((fract_mul << LPC_UART_FDR_MULVAL_Pos) & LPC_UART_FDR_MULVAL_Msk);
            MB_UART->LCR &= ~LPC_UART_LCR_DLAB_Msk;
            MB_UART->FCR = LPC_UART_FCR_FIFOEnable_Msk | LPC_UART_FCR_RXFIFOReset_Msk |
                LPC_UART_FCR_TXFIFOReset_Msk;
            MB_UART->FCR = LPC_UART_FCR_FIFOEnable_Msk;

            mbPortSerialInterruptEnable();
            g_uart1_inten = true;


            (volatile void)MB_UART->IIR;
        } else {
            init_ok = FALSE;
        }
    }

    g_mbport_mode = e_MBPortMode_Serial;

    return init_ok;
}

BOOL xMBPortSerialPutByte(CHAR ucByte) {
    MB_UART->THR = ucByte;
#ifdef MB_INDICATE_TX
    MB_INDICATE_TX();
#endif
    return TRUE;
}
