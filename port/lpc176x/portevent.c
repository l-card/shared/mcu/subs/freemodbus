#include <stdbool.h>

#include "port.h"

#include "mb.h"
#include "mbport.h"
#include "porttcp.h"


/*================================================================================================*/
static eMBEventType eQueuedEvent;
static bool xEventInQueue;
/*================================================================================================*/

/*================================================================================================*/
/*------------------------------------------------------------------------------------------------*/
BOOL xMBPortEventGet
    (
    eMBEventType *eEvent
    )
    {
    BOOL xEventHappened = FALSE;

    if (xEventInQueue)
        {
        *eEvent = eQueuedEvent;
        xEventInQueue = false;
        xEventHappened = TRUE;
        }
#if MB_TCP_ENABLED
    else if (g_mbport_mode == e_MBPortMode_TCP)
        {
        mbtcp_poll();
        }
#endif
    return xEventHappened;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL xMBPortEventInit
    (
    void
    )
    {
    xEventInQueue = false;
    return TRUE;
    }
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
BOOL xMBPortEventPost
    (
    eMBEventType eEvent
    )
    {
    xEventInQueue = true;
    eQueuedEvent = eEvent;
    return TRUE;
    }
/*------------------------------------------------------------------------------------------------*/
/*================================================================================================*/
